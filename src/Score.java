import java.util.HashMap;

class Score {
    private int playerOneScore;
    private int playerTwoScore;

    private HashMap scoreNames = Const.scoreNames();

    Score(int playerOneScore, int playerTwoScore) {
        this.playerOneScore = playerOneScore;
        this.playerTwoScore = playerTwoScore;
    }

    String tennisScore() {
        return playerOneScore + "-" + playerTwoScore + ": " + gameScore();
    }

    private boolean playerWon() {
        return (Math.abs(playerOneScore - playerTwoScore) > 1 && (playerOneScore > 3 || playerTwoScore > 3));
    }

    private String gameScore() {
        if (playerWon()) return ("Win for " + winningPlayer());
        if (Math.abs(playerOneScore - playerTwoScore) < 2 && (playerOneScore > 3 || playerTwoScore > 3))
            return "Advantage " + winningPlayer();
        if (playerOneScore == 3 && playerTwoScore == 3) return "Deuce";
        if (playerOneScore == playerTwoScore) return (scoreNames.get(playerOneScore) + "-All");
        return scoreNames.get(playerOneScore) + "-" + scoreNames.get(playerTwoScore);
    }

    private String winningPlayer() {
        return (playerOneScore > playerTwoScore) ? "player1" : "player2";
    }


}
