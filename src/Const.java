import java.util.HashMap;

class Const {
    private static final String ZERO_POINT = "Love";
    private static final String ONE_POINT = "Fifteen";
    private static final String TWO_POINT = "Thirty";
    private static final String THREE_POINT = "Forty";

    static HashMap scoreNames() {
        HashMap<Integer, String> scoreNames = new HashMap<Integer, String>();
        scoreNames.put(0, ZERO_POINT);
        scoreNames.put(1, ONE_POINT);
        scoreNames.put(2, TWO_POINT);
        scoreNames.put(3, THREE_POINT);
        return scoreNames;
    }
}
